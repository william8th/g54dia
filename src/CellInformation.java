import uk.ac.nott.cs.g54dia.library.Cell;
import uk.ac.nott.cs.g54dia.library.Station;


public class CellInformation implements Comparable<CellInformation> {
	private int distance = 0;
	public Cell c = null;
	public MPoint m = null;
	
	public CellInformation(int dist, Cell c, MPoint m) {
		this.distance = dist;
		this.c = c;
		this.m = m;
	}
	
	/*
	 * Update the distance of the node from the relative position of the tank
	 * @param origin The belief position of the tank
	 * @param direction The direction that the tank wants to head in
	 */
	public void updateDistance(MPoint origin, int direction) {
		this.distance = Util.getInstance().absDistance(origin, this.m);
	}
	
	public int getDistance() {
		return this.distance;
	}

	@Override
	public int compareTo(CellInformation n) {
		int compare = -this.distance + n.getDistance();
		if (this.c instanceof Station)
			return -compare;
		return compare;
	}
	
	@Override
	public boolean equals(Object o) {
		CellInformation n = (CellInformation)o;
		return (n.m.equals(this.m));
	}
	
	@Override
	public String toString() {
		return "Point: (" + this.m.x + "," + this.m.y + "); Distance:" + this.distance; 
	}
}
