
import uk.ac.nott.cs.g54dia.library.Action;
import uk.ac.nott.cs.g54dia.library.ActionFailedException;
import uk.ac.nott.cs.g54dia.library.Cell;
import uk.ac.nott.cs.g54dia.library.Environment;
import uk.ac.nott.cs.g54dia.library.OutOfFuelException;
import uk.ac.nott.cs.g54dia.library.Tanker;
import uk.ac.nott.cs.g54dia.library.TankerViewer;


public class Simulator {
	private static int DELAY = 0;
	private static int DURATION = 10 * 10000;
	
	public Environment env = new Environment(Tanker.MAX_FUEL/2-5);
	public Tanker myTanker = new IronTanker();
	public TankerViewer tv = new TankerViewer(myTanker);
	
	public void run() {
		
		tv.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		
		while(env.getTimestep() < DURATION) {
			env.tick();
			tv.tick(env);
			
			Cell[][] view = env.getView(myTanker.getPosition(), Tanker.VIEW_RANGE);
			Action a = myTanker.senseAndAct(view, env.getTimestep());
			
			try {
				a.execute(env, myTanker);
			} catch (OutOfFuelException ofe) {
				System.err.println("Tanker out of fuel!");
				break;
			} catch (ActionFailedException afe) {
				System.err.println(afe.getMessage());
				MySimulator.pause();
			}
			
			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException e) { 
				// Can't do anything useful
			}

		}
	}
}
