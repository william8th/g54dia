import uk.ac.nott.cs.g54dia.library.Cell;
import uk.ac.nott.cs.g54dia.library.Station;
import uk.ac.nott.cs.g54dia.library.Well;


public class Node {
	public Cell cell = null;
	public MPoint m = null;
	
	public Node(Cell c, MPoint m) {
		this.cell = c;
		this.m = m;
	}
	
	public int distanceFrom(MPoint x) {
		return Util.getInstance().absDistance(m, x);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if (this.cell instanceof Well)
			sb.append("Well ");
		else if (this.cell instanceof Station)
			sb.append("Station ");
		else
			sb.append("FuelPump ");
		
		sb.append(m);
		
		return sb.toString();
	}
}
