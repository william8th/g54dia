
public class MPoint {
	public int x = 0;
	public int y = 0;
	
	public MPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}
	
	@Override
	public boolean equals(Object o) {
		MPoint m = (MPoint)o;
		return (this.x == m.x && this.y == m.y);
	}
}
