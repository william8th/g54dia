import uk.ac.nott.cs.g54dia.library.Station;


public class StationTask implements Comparable<StationTask>{
	private Station s = null;
	public MPoint m = null;
	public int waterNeeded = 0;
	
	public StationTask(Station s, MPoint m) {
		this.s = s;
		this.m = m;
		
		assert(s.getTask() != null);
		
		this.waterNeeded = s.getTask().getRequired();
		assert(this.waterNeeded > 0);
	}
	
	public Station getStation() {
		return this.s;
	}
	
	@Override
	public int compareTo(StationTask s2) {
		return this.waterNeeded - s2.waterNeeded;
	}
}
