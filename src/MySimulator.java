import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import uk.ac.nott.cs.g54dia.library.*;


public class MySimulator {

	private static int DELAY = 50;
	private static int DURATION = 10 * 10000;
	
	private static InputStreamReader ir = new InputStreamReader(System.in);
	private static BufferedReader br = new BufferedReader(ir);
	
	public static void main(String[] args) {
		Environment env = new Environment(Tanker.MAX_FUEL/2-5);
		Tanker myTanker = new IronTanker();
		TankerViewer tv = new TankerViewer(myTanker);
		tv.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		
		while(env.getTimestep() < DURATION) {
			env.tick();
			tv.tick(env);
			
			Cell[][] view = env.getView(myTanker.getPosition(), Tanker.VIEW_RANGE);
			Action a = myTanker.senseAndAct(view, env.getTimestep());
			
			try {
				a.execute(env, myTanker);
			} catch (OutOfFuelException ofe) {
				System.err.println("Tanker out of fuel!");
				break;
			} catch (ActionFailedException afe) {
				System.err.println(afe.getMessage());
				MySimulator.pause();
			}
			
			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException e) { 
				// Can't do anything useful
			}

		}
	}
	
	public static void pause() {
		while (true) {
			try {
				if (!MySimulator.br.readLine().equals(null)) break;
			} catch (IOException ioe) {
				System.out.println("Failed to pause");
				ioe.printStackTrace();
			}
		}
	}


}
