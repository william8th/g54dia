import java.util.ArrayList;
import java.util.Collections;

import uk.ac.nott.cs.g54dia.library.*;


public class MyTanker extends Tanker {
	private static final MPoint ORIGIN = new MPoint(0, 0);
	
	// States
	private static final int FORAGE = 0;
	private static final int LOAD = 1;
	private static final int DELIVER = 2;
	private static final int REFUEL = 3;
	private int state = FORAGE;
	
	// Forage states
	private static final int FORAGE_START = 0;
	private static final int FORAGE_END = 1;
	private int forageState = FORAGE_START;
	
	// Belief state
	private int bx = 0;  // Belief x
	private int by = 0;  // Belief y
	private int direction = 0;
	
	private ArrayList<CellInformation> wells = new ArrayList<>();
	private ArrayList<CellInformation> stations = new ArrayList<>();
	private ArrayList<StationTask> stationTasks = new ArrayList<>();
	
	private MPoint target = null;
	private Station targetStation = null;
	

	// Forage variables
	private int forageDist = 0;
	private int forageDistTarget = (int)Tanker.VIEW_RANGE/2;
	private int forageDistTargetSum = 6 + (int)(Math.random() * 4);  // 6 + [0-3]
	private int forageDirection = MoveAction.NORTHEAST;
	
	@Override
	public Action senseAndAct(Cell[][] view, long timestep) {
		for (int x = 0; x < view.length; x++) {
			for (int y = 0; y < view[x].length; y++) {
				Cell c = view[x][y];
				MPoint m = new MPoint(x-Tanker.VIEW_RANGE+this.bx, Tanker.VIEW_RANGE-y+this.by);
				
				if (!this.existsCell(c)) {
					this.addCell(c, m);
				}
				
				// Check to see if it's a station and if it has a task
				if (c instanceof Station) {
					Station s = (Station)c;
					if (s.getTask() != null && !this.existsStationTask(m)) {
						StationTask st = new StationTask(s, m);
						this.stationTasks.add(st);
					}
				}
			}
		}
		
		// Resort the array list
		Collections.sort(this.wells);
		Collections.sort(this.stations);
		Collections.sort(this.stationTasks);
		
		if (this.state == MyTanker.DELIVER && stationTasks.isEmpty()) {
			// Have to forage for tasks
			this.state = MyTanker.FORAGE;
			this.targetStation = null;
			this.target = null;
		}
		
		if (this.getFuelLevel()-1 <= Util.getInstance().absDistance(new MPoint(this.bx, this.by), ORIGIN)) {
			this.state = MyTanker.REFUEL;
			this.targetStation = null;
			this.target = null;
		}
		
		switch (this.state) {
		case MyTanker.FORAGE:
			switch (this.forageState) {
			case MyTanker.FORAGE_START:
				if (this.getFuelLevel()-1 > Util.getInstance().absDistance(new MPoint(this.bx, this.by), ORIGIN)) {
					if (this.forageDist >= this.forageDistTarget) {
						this.forageDist = 0;
						this.forageDistTarget += this.forageDistTargetSum;
						this.forageDistTargetSum += (int)(Math.random() * 4);
						
						int newDirection = 0;
						switch (this.forageDirection) {
						case MoveAction.NORTHEAST:
							newDirection = MoveAction.SOUTHEAST;
							break;
						case MoveAction.SOUTHEAST:
							newDirection = MoveAction.SOUTHWEST;
							break;
						case MoveAction.SOUTHWEST:
							newDirection = MoveAction.NORTHWEST;
							break;
						case MoveAction.NORTHWEST:
							newDirection = MoveAction.NORTHEAST;
							break;
						}
						this.forageDirection = newDirection;
					}
					
					this.forageDist++;
					this.direction = this.forageDirection;
				} else {
					this.forageState = MyTanker.FORAGE_END;
				}
				break;

			case MyTanker.FORAGE_END:
				try {
					this.direction = this.moveToPoint(ORIGIN);
				} catch (Exception e) {
					// At fuel pump
//					this.printWells();
//					this.printStations();
					
					if (this.getWaterLevel() > 0)
						this.state = MyTanker.DELIVER;
					else
						this.state = MyTanker.LOAD;
					
					this.forageState = MyTanker.FORAGE_START;
					this.forageDistTargetSum = 0;
					return new RefuelAction();
				}
				break;
			}
			break;
			
			
		case MyTanker.LOAD:
			if (!wells.isEmpty()) {
				target = wells.get(0).m;
			}
				
			try {
				this.direction = moveToPoint(target);
			} catch (Exception e) {
				// Reached target
				this.state = MyTanker.DELIVER;
				System.out.println("Water loaded");
				return new LoadWaterAction();
			}
			break;
			
		case MyTanker.DELIVER:
			if (!stationTasks.isEmpty() && targetStation == null) {
				StationTask st = stationTasks.get(0);
				
				if (st.waterNeeded > this.getWaterLevel()) {
					this.state = MyTanker.LOAD;
					this.targetStation = null;
					this.target = null;
					break;
				} else {
					this.targetStation = st.getStation();
					this.target = st.m;
//					System.out.println("Station tasks:" + stationTasks.size());
				}
			}
			
			try {
				this.direction = moveToPoint(target);
			} catch (Exception e) {
				DeliverWaterAction a = new DeliverWaterAction(this.targetStation.getTask());
//				System.out.println("ST before:" + stationTasks.size());
				this.removeStationTask(this.target);
//				System.out.println("ST after:" + stationTasks.size());
				this.targetStation = null;
				this.target = null;
				this.state = MyTanker.DELIVER;
				System.out.println("Water delivered");
				return a;
			}
			break;
			
		case MyTanker.REFUEL:
			this.target = MyTanker.ORIGIN;
			try {
				this.direction = this.moveToPoint(this.target);
			} catch (Exception e) {
				// At origin
				if (this.getWaterLevel() < Tanker.MAX_WATER)
					this.state = MyTanker.LOAD;
				else
					this.state = MyTanker.DELIVER;
				
				System.out.println("Refuelled");
				return new RefuelAction();
			}
			break;
		}

//		this.direction = (int)(Math.random() * 8);
		this.changeBelief(this.direction);
		return new MoveAction(this.direction);
	}
	
	private boolean existsStationTask(MPoint m) {
		for (StationTask x : this.stationTasks) {
			if (x.m.equals(m)) return true;
		}
		return false;
	}
	
	private void removeStationTask(MPoint m) {
		int i = 0;
		for (StationTask st : this.stationTasks) {
			if (st.m.equals(m)) break;
			i++;
		}
		this.stationTasks.remove(i);
	}
	
	/*
	 * Calculate which direction to move the agent towards
	 * @param m A belief position of the target
	 * @return Direction
	 */
	private int moveToPoint(MPoint m) throws Exception {
//		System.out.println("Trying to move to " + m);
		
		if (m.x == this.bx && m.y == this.by) throw new Exception("Already there!");
		
		int dx = m.x - this.bx;
        int dy = m.y - this.by;
        boolean north, south, east, west;
        north = south = east = west = false;
        
        if (dx < 0) {
        	west = true;
        } else if (dx > 0) {
        	east = true;
        }
        
        if (dy < 0) {
        	south = true;
        } else if (dy > 0) {
        	north = true;
        }
        
        int d = 0;
        if (north) d = MoveAction.NORTH;
        if (south) d = MoveAction.SOUTH;
        if (east) d = MoveAction.EAST;
        if (west) d = MoveAction.WEST;
        
        // We would like to rewrite the value if it wants to move diagonally
        if (north && east) d = MoveAction.NORTHEAST;
        if (north && west) d = MoveAction.NORTHWEST;
        if (south && east) d = MoveAction.SOUTHEAST;
        if (south && west) d = MoveAction.SOUTHWEST;
        
        return d;
	}
	
	/*
	 * Change the agent's belief state including its own position and the positions of the special cells
	 * @param direction Direction that the agent is going to move towards
	 */
	private void changeBelief(int direction) {
		switch (direction) {
		case MoveAction.NORTH:
			this.by++;
			break;
		case MoveAction.SOUTH:
			this.by--;
			break;
		case MoveAction.EAST:
			this.bx++;
			break;
		case MoveAction.WEST:
			this.bx--;
			break;
		case MoveAction.NORTHEAST:
			this.bx++;
			this.by++;
			break;
		case MoveAction.NORTHWEST:
			this.bx--;
			this.by++;
			break;
		case MoveAction.SOUTHEAST:
			this.bx++;
			this.by--;
			break;
		case MoveAction.SOUTHWEST:
			this.bx--;
			this.by--;
			break;
		}
		
		// Update relative distance of wells and stations
		MPoint currentPosition = new MPoint(this.bx, this.by);
		
		for (CellInformation n : wells) {
			n.updateDistance(currentPosition, direction);
		}
		
		for (CellInformation n : stations) {
			n.updateDistance(currentPosition, direction);
		}
	}
	
	/*
	 * Add special cell to list of known cells be it wells or stations
	 * @param c Cell to add
	 * @param m Relative position of the cell to the tank
	 */
	private void addCell(Cell c, MPoint m) {
		CellInformation n = new CellInformation(Util.getInstance().absDistance(new MPoint(this.bx, this.by), m), c, m);
		
		if (c instanceof Well) {
			wells.add(n);
//			System.out.println("Spotted well at " + c.getPoint() + ", added at " + m);
		} else if (c instanceof Station) {
			stations.add(n);
//			System.out.println("Spotted station at " + c.getPoint() + ", added at " + m);
		}
	}
	
	/*
	 * Check if cell already exists in known-cell-lists
	 */
	private boolean existsCell(Cell c) {
		boolean exists = false;
		Point p = c.getPoint();
		ArrayList<CellInformation> al = null;
		
		if (c instanceof Well)
			al = wells;
		else if (c instanceof Station)
			al = stations;
		
		if (al == null) return true;  // Just an empty cell, return saying that we don't need to remember it
		
		for (CellInformation n : al) {
			if (n.c.getPoint().equals(p)) {
				exists = true;
				break;
			}
		}
		
		return exists;
	}
	
	private void printWells() {
		for (CellInformation n : wells) {
			System.out.println("W" + n);
		}
	}
	
	
	private void printStations() {
		for (CellInformation n : stations) {
			System.out.println("S" + n);
		}
	}
	
}
