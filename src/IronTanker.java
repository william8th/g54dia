import java.util.ArrayList;
import java.util.Collections;

import uk.ac.nott.cs.g54dia.library.*;


public class IronTanker extends Tanker {
	private static final MPoint ORIGIN = new MPoint(0, 0);
	
	// Belief state
	private int bx = 0;  // Belief x
	private int by = 0;  // Belief y
	private int direction = 0;
	
	// List of wells and stations explored. We only remember stations with a task
	private ArrayList<CellInformation> wells = new ArrayList<>();
	private ArrayList<CellInformation> stations = new ArrayList<>();
	
	private Plan currentPlan = null;    // Can change if there's a better plan 
	private ArrayList<Node> currentSteps = null;
	
	private MPoint target = null;    // The target destination of the tanker
	
	@Override
	public Action senseAndAct(Cell[][] view, long timestep) {
		IronTanker.currentPos = new MPoint(this.bx, this.by);

		for (int x = 0; x < view.length; x++) {
			for (int y = 0; y < view[x].length; y++) {
				Cell c = view[x][y];
				MPoint m = new MPoint(x-Tanker.VIEW_RANGE+this.bx, Tanker.VIEW_RANGE-y+this.by);
				
				if (c instanceof Station || !this.existsCell(c)) {
					this.addCell(c, m);  // We try to add all stations with tasks and wells only
				}
			}
		}
		
		// Sort so that we know which well and station is closest to us
		Collections.sort(this.wells);
		Collections.sort(this.stations);
		
		if (this.forageState != IronTanker.FORAGE_STATE_IDLE) {
			this.forage();
		}
		
		if (this.forageState == IronTanker.FORAGE_STATE_REFUEL) {
			if (this.stations.size() == 0)
				this.forageState = IronTanker.FORAGE_STATE_START;  // Still can't find station with a task, forage again
			else
				this.forageState = IronTanker.FORAGE_STATE_IDLE;

			return new RefuelAction();
		}
		
		if (this.stations.size() > 0) {
			Plan candidatePlan = this.backtrack(IronTanker.currentPos, this.getFuelLevel(), this.getWaterLevel(), 0, new ArrayList<MPoint>());
			
			if (this.currentPlan == null && candidatePlan != null) {
				this.currentPlan = candidatePlan;
				this.currentSteps = this.currentPlan.steps;
				System.out.println(this.currentPlan);
			} else if (this.currentPlan == null && candidatePlan == null) {
				// If we have a list of stations yet we still cannot find a plan, we forage
				this.forageState = IronTanker.FORAGE_STATE_START;
				this.forage();
			} else if (this.currentPlan != null && candidatePlan != null && candidatePlan.score > this.currentPlan.score) {
				this.currentPlan = candidatePlan;
				this.currentSteps = this.currentPlan.steps;
				System.out.println("Change of plan:" + this.currentPlan);
			}
		}
		
		if (this.currentSteps != null && this.currentSteps.size() > 0) {
			int lastIndex = this.currentSteps.size()-1;
			MPoint possibleTarget = this.currentSteps.get(lastIndex).m;  // Get the last one on the list
			MPoint currentPosition = IronTanker.currentPos;
			
			if (possibleTarget.equals(currentPosition)) {
				// We've reached our destination, do what we need to do
				Action specialAction = null;
				for (Node n : this.currentPlan.steps) {
					if (n.m.equals(currentPosition)) {
						// We want to know what kind of cell it is to return the relevant action
						if (n.cell instanceof Well && this.getWaterLevel() < Tanker.MAX_WATER) {
							specialAction = new LoadWaterAction();
						} else if (n.cell instanceof Station) {
							Station s = (Station)n.cell;
							specialAction = new DeliverWaterAction(s.getTask());
							this.removeStationWithPoint(n.m);
						} else if (n.cell == null && this.getFuelLevel() < Tanker.MAX_FUEL) {  // FuelPump is set to null in the planning
							specialAction = new RefuelAction();
						}
					}
				}
				
				// Remove the final step from current steps
				this.currentSteps.remove(lastIndex);
				
				// The tank now needs to move to the next target
				lastIndex--;
				if (lastIndex >= 0) {
					possibleTarget = this.currentSteps.get(lastIndex).m;
				} else {
					// End of plan
					this.currentPlan = null;
					this.currentSteps = null;
					this.target = null;
					possibleTarget = null;
					
					if (this.stations.size() == 0)
						this.forageState = IronTanker.FORAGE_STATE_START;
				}
				
				if (specialAction != null) return specialAction;
			}
			
			this.target = possibleTarget;
		}
		
		if (this.target != null) {
			try {
				this.direction = this.moveToPoint(this.target);
			} catch (Exception e) {
				e.printStackTrace();
				MySimulator.pause();
			}
		}
		
		this.changeBelief(this.direction);
		return new MoveAction(this.direction);
	}
	
	private static MPoint currentPos = IronTanker.ORIGIN;
	private Plan backtrack(final MPoint previousPos, final int availableCost, final int availableWater, final int prevWater, final ArrayList<MPoint> visited) {
		// Calculate score and cost
		ArrayList<Plan> candidatePlans = new ArrayList<>();
		
		for (CellInformation ci : this.stations) {
			Plan p = new Plan();
			boolean isWellAdded = false;
			
			// Add fuel pump if we are creating a new plan
			if (previousPos.equals(IronTanker.currentPos)) {
				p.add(new Node(null,IronTanker.ORIGIN));  // All plans end with the fuel pump as its destination
			}
			
			Station s = (Station)ci.c;
			if (visited.contains(ci.m))
				continue;    // Skip if this station has been visited

			
			int waterRequired = s.getTask().getRequired();
			if (waterRequired + prevWater > Tanker.MAX_WATER) {
				CellInformation bestWell = this.getBestWellFrom(previousPos);
				if (bestWell == null) return null;
				p.add(new Node(bestWell.c, bestWell.m));
			}
			
			if (p.cost > availableCost) return null;    // If we cannot make it to the well for the previous station, discard everything
			
			p.add(new Node(s, ci.m));  // Add to visit station
			if (p.cost > availableCost) return null;    // We can't make it to the next station, discard everything
			
			if (waterRequired + prevWater > availableWater) {
				// Need to add well in path
				// Select the one shortest from current position or shortest from target station
				CellInformation bestWell = this.getBestWellFrom(ci.m);
				
				// If we don't have any wells in memory, we cannot plan
				if (bestWell == null) return null;
				
				p.add(new Node(bestWell.c, bestWell.m));
				isWellAdded = true;
			}
			
			
			MPoint lastPosInPlan = p.steps.get(p.steps.size()-1).m;
			int current2StartCost = Util.getInstance().absDistance(IronTanker.currentPos, lastPosInPlan);
			int thisPlan2PreviousPlanCost = 0;
			if (p.steps.size() > 0)
				thisPlan2PreviousPlanCost = Util.getInstance().absDistance(p.steps.get(0).m, previousPos);
			
			int calculatedCost = availableCost - p.cost - current2StartCost - thisPlan2PreviousPlanCost;
			if (calculatedCost <= 0) {
				// Reached the limits of tank
				continue;
			} else {
				visited.add(ci.m);
				int accumulateWater = (isWellAdded) ? 0 : waterRequired + prevWater;
				@SuppressWarnings("unchecked")
				Plan bestChildPlan = this.backtrack(lastPosInPlan, availableCost-p.cost-thisPlan2PreviousPlanCost, availableWater-waterRequired, accumulateWater, (ArrayList<MPoint>)visited.clone());
				p.merge(bestChildPlan);
				candidatePlans.add(p);
			}
		}
		
		
		
		if (candidatePlans.size() == 0)
			return null;
		else {
			// Rank the candidate plans
			Collections.sort(candidatePlans, Collections.reverseOrder());
			return candidatePlans.get(0);
		}
	}

	
	/*
	 * Try to get a best well for a station. Note that it may return null if there's not a single well
	 * known to the tanker. We only consider a well nearest to the tank's current position or the well
	 * nearest to the destination station.
	 * @param des The destination, or rather, the station with a task
	 * @return A CellInformation instance containing the best well. May return null.
	 */
	private CellInformation getBestWellFrom(MPoint des) {
		CellInformation well2cur = this.getNearestWellFrom(IronTanker.currentPos);
		CellInformation well2station = this.getNearestWellFrom(des);
		
		if (well2cur == null) return well2station;
		
		if (Util.getInstance().absDistance(IronTanker.currentPos, well2cur.m) < Util.getInstance().absDistance(des, well2station.m))
			return well2cur;
		else
			return well2station;
	}
	
	
	/*
	 * Get the nearest well from a certain point
	 * @param m A point where you want the nearest well from
	 * @return A CellInformation instance containing the nearest well from the point provided
	 */
	private CellInformation getNearestWellFrom(MPoint m) {
		CellInformation min = null;
		int distance = Integer.MAX_VALUE;
		for (CellInformation ci : this.wells) {
			int c2mDist = Util.getInstance().absDistance(ci.m, m);
			if (c2mDist < distance) {
				distance = c2mDist;
				min = ci;
			}
		}
		
		return min;
	}
	
	// Forage variables
	private int forageState = 1;
	private static final int FORAGE_STATE_IDLE = 0;
	private static final int FORAGE_STATE_START = 1;
	private static final int FORAGE_STATE_END = 2;
	private static final int FORAGE_STATE_REFUEL = 3;
	private int forageDist = 0;
	private int forageDistTarget = (int)Tanker.VIEW_RANGE/2;
	private int forageDistTargetSum = 6 + (int)(Math.random() * 4);  // 6 + [0-3]
	private int forageDirection = MoveAction.NORTHEAST;
	
	/*
	 * Algorithm to direct tanker to forage the environment
	 */
	private void forage() {
		switch (this.forageState) {
		case IronTanker.FORAGE_STATE_START:
			if (this.getFuelLevel() > Util.getInstance().absDistance(IronTanker.currentPos, ORIGIN)) {
				if (this.forageDist >= this.forageDistTarget) {
					this.forageDist = 0;
					this.forageDistTarget += this.forageDistTargetSum;
					this.forageDistTargetSum += (int)(Math.random() * 4);
					
					int newDirection = 0;
					switch (this.forageDirection) {
					case MoveAction.NORTHEAST:
						newDirection = MoveAction.SOUTHEAST;
						break;
					case MoveAction.SOUTHEAST:
						newDirection = MoveAction.SOUTHWEST;
						break;
					case MoveAction.SOUTHWEST:
						newDirection = MoveAction.NORTHWEST;
						break;
					case MoveAction.NORTHWEST:
						newDirection = MoveAction.NORTHEAST;
						break;
					}
					this.forageDirection = newDirection;
				}
				
				this.forageDist++;
				this.direction = this.forageDirection;
				
				break;
			} else {
				this.forageState = IronTanker.FORAGE_STATE_END;
			}

		case IronTanker.FORAGE_STATE_END:
			try {
				this.direction = this.moveToPoint(ORIGIN);
			} catch (Exception e) {
				// At fuel pump
				this.forageState = IronTanker.FORAGE_STATE_REFUEL;
				this.forageDistTargetSum = 0;
			}
			break;
		}
	}
	
	/*
	 * We want to remove the station when a task is completed
	 * @param m The station to be removed. This MPoint acts as the unique ID identifying the station
	 */
	private void removeStationWithPoint(MPoint m) {
		int i = 0;
		for (CellInformation ci : this.stations) {
			if (ci.m.equals(m)) {
				break;
			}
			i++;
		}
		
		this.stations.remove(i);
	}
	
	/*
	 * Calculate which direction to move the agent towards
	 * @param m A belief position of the target
	 * @return Direction
	 */
	private int moveToPoint(MPoint m) throws Exception {
		if (m.x == this.bx && m.y == this.by) {
			throw new Exception("Already there!");
		}
		
		int dx = m.x - this.bx;
        int dy = m.y - this.by;
        boolean north, south, east, west;
        north = south = east = west = false;
        
        if (dx < 0) {
        	west = true;
        } else if (dx > 0) {
        	east = true;
        }
        
        if (dy < 0) {
        	south = true;
        } else if (dy > 0) {
        	north = true;
        }
        
        int d = 0;
        if (north) d = MoveAction.NORTH;
        if (south) d = MoveAction.SOUTH;
        if (east) d = MoveAction.EAST;
        if (west) d = MoveAction.WEST;
        
        // We would like to rewrite the value if it wants to move diagonally
        if (north && east) d = MoveAction.NORTHEAST;
        if (north && west) d = MoveAction.NORTHWEST;
        if (south && east) d = MoveAction.SOUTHEAST;
        if (south && west) d = MoveAction.SOUTHWEST;
        
        return d;
	}
	
	
	/*
	 * Change the agent's belief state including its own position and the positions of the special cells
	 * @param direction Direction that the agent is going to move towards
	 */
	private void changeBelief(int direction) {
		switch (direction) {
		case MoveAction.NORTH:
			this.by++;
			break;
		case MoveAction.SOUTH:
			this.by--;
			break;
		case MoveAction.EAST:
			this.bx++;
			break;
		case MoveAction.WEST:
			this.bx--;
			break;
		case MoveAction.NORTHEAST:
			this.bx++;
			this.by++;
			break;
		case MoveAction.NORTHWEST:
			this.bx--;
			this.by++;
			break;
		case MoveAction.SOUTHEAST:
			this.bx++;
			this.by--;
			break;
		case MoveAction.SOUTHWEST:
			this.bx--;
			this.by--;
			break;
		}
		
		// Update relative distance of wells and stations
		MPoint currentPosition = new MPoint(this.bx, this.by);
		
		for (CellInformation n : wells) {
			n.updateDistance(currentPosition, direction);
		}
		
		for (CellInformation n : stations) {
			n.updateDistance(currentPosition, direction);
		}
	}
	
	
	/* Add special cell to list of known cells be it wells or stations.
	 * Note that stations are added (or replaced) no matter what. We want to get the latest list of
	 * stations with their tasks. It is also important to know that this algorithm will not acknowledge
	 * cells beyond the reach of the tanker's mileage.
	 * @param c Cell to add
	 * @param m Relative position of the cell to the tank
	 */
	private final static int MAX_REACH = Tanker.MAX_FUEL/2 -1;
	private void addCell(Cell c, MPoint m) {
		// We do not acknowledge the existence of cells beyond reach
		if (Util.getInstance().absDistance(IronTanker.ORIGIN, m) >= IronTanker.MAX_REACH) return;
		
		
		CellInformation n = new CellInformation(Util.getInstance().absDistance(IronTanker.currentPos, m), c, m);
		
		if (c instanceof Well) {
			wells.add(n);
		} else if (c instanceof Station) {
			Station s = (Station)c;
			if (s.getTask() != null) {
				// We have to check if the station is known to carry a task so we can replace it
				boolean exists = false;
				int i = 0;
				for (CellInformation ci : this.stations) {
					if (ci.m.equals(m)) {
						exists = true;
						break;
					}
					i++;
				}
				if (exists) this.stations.remove(i);
				stations.add(n);  // Add or replace station only if it has a task
			}
		}
	}
	
	
	/*
	 * Check if cell already exists in known-cell-lists. Note that this only checks for list of wells known.
	 * We do not check for stations because we want to replace the stations no matter what. Check out the
	 * addCell(Cell, MPoint) method.
	 */
	private boolean existsCell(Cell c) {
		if (! (c instanceof Well)) return true;  // Just an empty cell, return true saying that we don't need to remember it
		
		boolean exists = false;
		Point p = c.getPoint(); 
		
		for (CellInformation n : this.wells) {
			if (n.c.getPoint().equals(p)) {
				exists = true;
				break;
			}
		}
		
		return exists;
	}
}
