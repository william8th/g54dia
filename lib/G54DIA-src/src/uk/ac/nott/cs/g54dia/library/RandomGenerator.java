package uk.ac.nott.cs.g54dia.library;
import java.util.Random;

/*
 * Author: William Heng <william@williamheng.com>
 * Date: 10th of February 2015
 */

public class RandomGenerator {

	private static RandomGenerator instance = null;
	
	private boolean isSeedSet = false;
	private int seed = 42;  // 42 is the answer to everything
	private Random r;
	
	protected RandomGenerator() {
		r = new Random();
	}
	
	public static RandomGenerator getInstance() {
		if (instance == null) {
			instance = new RandomGenerator();
		}
		
		return instance;
	}
	
	public synchronized void setSeed(int seed) {
		if (this.isSeedSet) {
			// Warn that the seed is resetting
			System.out.println("Warning: Seed has been set to " + this.seed + " previously. Setting seed to " + seed);
		}
		isSeedSet = true;
		r.setSeed(seed);
		this.seed = seed;
	}
	
	public double nextDouble() {
		return r.nextDouble();
	}
	
}