import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ BeliefMapTest.class, BeliefPositionTest.class })
public class AllTests {
	public final static int DELAY = 0;
	public final static int TIMESTEP = 10000;
	public final static int TEST_FUEL = 10000;
}
