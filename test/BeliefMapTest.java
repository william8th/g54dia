import java.lang.reflect.*;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uk.ac.nott.cs.g54dia.library.*;

public class BeliefMapTest {
	
	private Environment env = null;
	private MyTanker t = null;
	private TankerViewer tv = null;
	
	private ArrayList<CellInformation> wells = null;
	private ArrayList<CellInformation> stations = null;
	
	@Before
	public void setUp() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		env = new Environment(Tanker.MAX_FUEL/2-5);
		t = new MyTanker();
		tv = new TankerViewer(t);
		tv.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		
		// Set tanker to have 10000 fuel
		Field fuelField = Tanker.class.getDeclaredField("fuelLevel");
		fuelField.setAccessible(true);
		fuelField.set(t, AllTests.TEST_FUEL);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void beliefStateTest() throws Exception {
		Field wellsField = MyTanker.class.getDeclaredField("wells");
		Field stationsField = MyTanker.class.getDeclaredField("stations");
		wellsField.setAccessible(true);
		stationsField.setAccessible(true);
		
		while(env.getTimestep() < AllTests.TIMESTEP) {
			env.tick();
			tv.tick(env);
			
			Cell[][] view = env.getView(t.getPosition(), Tanker.VIEW_RANGE);
			Action a = t.senseAndAct(view, env.getTimestep());
			
			try {
				a.execute(env, t);
			} catch (Exception e) {
				// Test ignores agent's exceptions e.g. failed to load water or move to point
				break;
			}
			
			Thread.sleep(AllTests.DELAY);
		}
		
		wells = (ArrayList<CellInformation>)wellsField.get(t);
		for (CellInformation n : wells) {
			Point p = n.c.getPoint();
			Field pXField = Point.class.getDeclaredField("x");
			Field pYField = Point.class.getDeclaredField("y");
			pXField.setAccessible(true);
			pYField.setAccessible(true);
			int px = (int)pXField.get(p);
			int py = (int)pYField.get(p);

			Assert.assertEquals(px, n.m.x);
			Assert.assertEquals(py, n.m.y);
		}
		
		stations = (ArrayList<CellInformation>)stationsField.get(t);
		for (CellInformation n : stations) {
			Point p = n.c.getPoint();
			Field pXField = Point.class.getDeclaredField("x");
			Field pYField = Point.class.getDeclaredField("y");
			pXField.setAccessible(true);
			pYField.setAccessible(true);
			int px = (int)pXField.get(p);
			int py = (int)pYField.get(p);

			Assert.assertEquals(px, n.m.x);
			Assert.assertEquals(py, n.m.y);
		}
		
		/*
		// Test to see if the belief state is correct
		// Wells
		for (Entry<Node, Point> e : wells.entrySet()) {
			Node n = e.getKey();
			Point p = e.getValue();
			Field pXField = p.getClass().getDeclaredField("x");
			Field pYField = p.getClass().getDeclaredField("y");
			pXField.setAccessible(true);
			pYField.setAccessible(true);
			int px = (int)pXField.get(p);
			int py = (int)pYField.get(p);
			System.out.println(px + "," + py + "; " + n.m.x + "," + n.m.y);
			Assert.assertEquals(px, n.m.x);
			Assert.assertEquals(py, n.m.y);
		}
		
		// Test to see if the belief state is correct
		// Stations
		for (Entry<Node, Point> e : stations.entrySet()) {
			Node n = e.getKey();
			Point p = e.getValue();
			Field pXField = p.getClass().getDeclaredField("x");
			Field pYField = p.getClass().getDeclaredField("y");
			pXField.setAccessible(true);
			pYField.setAccessible(true);
			int px = (int)pXField.get(p);
			int py = (int)pYField.get(p);
			System.out.println(px + "," + py + "; " + n.m.x + "," + n.m.y);
			Assert.assertEquals(px, n.m.x);
			Assert.assertEquals(py, n.m.y);
		}
		*/
	}
	
	

}
