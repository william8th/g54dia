import java.lang.reflect.Field;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uk.ac.nott.cs.g54dia.library.Action;
import uk.ac.nott.cs.g54dia.library.ActionFailedException;
import uk.ac.nott.cs.g54dia.library.Cell;
import uk.ac.nott.cs.g54dia.library.Environment;
import uk.ac.nott.cs.g54dia.library.Point;
import uk.ac.nott.cs.g54dia.library.Tanker;
import uk.ac.nott.cs.g54dia.library.TankerViewer;


public class BeliefPositionTest {

	private Environment env = null;
	private MyTanker t = null;
	private TankerViewer tv = null;
	
	@Before
	public void setUp() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		env = new Environment(Tanker.MAX_FUEL/2-5);
		t = new MyTanker();
		tv = new TankerViewer(t);
		tv.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		
		// Set tanker to have 10000 fuel
		Field fuelField = Tanker.class.getDeclaredField("fuelLevel");
		fuelField.setAccessible(true);
		fuelField.set(t, AllTests.TEST_FUEL);
	}

	/*
	 * Test to see if the tank's belief position is correct compared to the absolute coordinates in the simulator
	 */
	@Test
	public void beliefPositionTest() throws InterruptedException, ActionFailedException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		while(env.getTimestep() < AllTests.TIMESTEP) {
			env.tick();
			tv.tick(env);
			
			Point p = t.getPosition();
			Field pXField = Point.class.getDeclaredField("x");
			Field pYField = Point.class.getDeclaredField("y");
			pXField.setAccessible(true);
			pYField.setAccessible(true);
			int px = (int)pXField.get(p);
			int py = (int)pYField.get(p);
			
			Field bXField = MyTanker.class.getDeclaredField("bx");
			Field bYField = MyTanker.class.getDeclaredField("by");
			bXField.setAccessible(true);
			bYField.setAccessible(true);
			int bx = (int)bXField.get(t);
			int by = (int)bYField.get(t);
			Assert.assertEquals(bx, px);
			Assert.assertEquals(by, py);
			
			Cell[][] view = env.getView(t.getPosition(), Tanker.VIEW_RANGE);
			Action a = t.senseAndAct(view, env.getTimestep());
			
			try {
				a.execute(env, t);
			} catch (Exception e) {
				// Test ignores agent's exceptions e.g. failed to load water or move to point
				break;
			}
			
			Thread.sleep(AllTests.DELAY);
		}
	}

}
