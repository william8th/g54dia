import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import uk.ac.nott.cs.g54dia.library.*;


public class AverageRun {
	
	private Simulator sim = null;
	private final static int NUM_OF_RUN = 10;
	private final static int DURATION = 10 * 10000;
	private long[] scores = new long[AverageRun.NUM_OF_RUN];
	
	@Before
	public void setUp() throws Exception {
		sim = new Simulator();
	}

	@Test
	public void test() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method scoreMethod = Tanker.class.getDeclaredMethod("getScore", null);
		scoreMethod.setAccessible(true);
		
		final long target = new Long("7000000000");
		boolean exit = false;
		
		for (int i = 0; i < AverageRun.NUM_OF_RUN; i++) {
			this.sim.run();
			this.scores[i] = (long) scoreMethod.invoke(this.sim.myTanker, null);
			
			if (this.sim.env.getTimestep() < DURATION) {
				exit = true;
				break;
			}
			
			this.sim = new Simulator();
			
			if (this.scores[i] < target) {
				exit = true;
				break;
			}
		}
		
		long sum = 0;
		for (int i = 0; i < this.scores.length; i++) {
			System.out.println("Run " + (i+1) + ": " + this.scores[i]);
			sum += this.scores[i];
		}
		
		long average = sum/AverageRun.NUM_OF_RUN;
		System.out.println("Average:" + average);

	
		
		while(true);
	}

}
